#####################################################################################################
AUTHORIZATION CODE
localhost:8080/oauth/authorize?response_type=code&client_id=client2&scope=read
http://localhost:8080/oauth/token?grant_type=authorization_code&code=bXGyq7&scope=read

code value - one given in redirect uri



######################################################################################################
PASSWORD - typical
http://localhost:8080/oauth/token?grant_type=password&username=john&password=12345&scope=read


######################################################################################################
REFRESH TOKEN
http://localhost:8080/oauth/token?grant_type=refresh_token&scope=read&refresh_token=b4f42df0-4b58-4580-bc53-6d2b3230f4ed
scope - as it is
refresh_token - value -> from any given value

need to add uds to configuration endpoints